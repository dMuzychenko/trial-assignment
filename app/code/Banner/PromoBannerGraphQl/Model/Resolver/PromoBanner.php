<?php

declare(strict_types=1);

namespace Banner\PromoBannerGraphQl\Model\Resolver;

use Banner\PromoBanner\Helper\Data as BannerHelper;
use Banner\PromoBanner\Model\Banner;
use Banner\PromoBanner\Model\ResourceModel\Banner\CollectionFactory;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

class PromoBanner implements ResolverInterface
{
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var array
     */
    private $dataKeys;
    /**
     * @var BannerHelper
     */
    private $bannerHelper;

    /**
     * PromoBanner constructor.
     * @param CollectionFactory $collectionFactory
     * @param BannerHelper $bannerHelper
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        BannerHelper $bannerHelper
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->bannerHelper = $bannerHelper;
        $this->dataKeys = [
            'banner_id',
            'title',
            'banner_text',
            'popup_text',
            'show_once'
        ];
    }

    /**
     * @inheritDoc
     */
    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    {
        if (isset($value[$field->getName()])) {
            return $value[$field->getName()];
        }

        return $this->getAvailableBanners();
    }

    public function getAvailableBanners()
    {
        $banners = $this->collectionFactory->create();

        $result = [];

        /** @var Banner $banner */
        foreach ($banners as $banner) {
            if (!$this->bannerHelper->isBannerActive($banner->getDateStart(), $banner->getDateEnd())) {
                continue;
            }

            $data = $banner->toArray($this->dataKeys);

            $endTimestamp = \DateTime::createFromFormat(
                'Y-m-d H:i:s',
                $banner->getDateEnd(),
                (new \DateTimeZone('GMT'))
            )->getTimestamp();
            $data['date_end'] = $endTimestamp;

            $result[] = $data;
        }

        return $result;
    }
}
