<?php

namespace Banner\PromoBanner\Ui\Listing\Column;

class ViewActions extends \Magento\Ui\Component\Listing\Columns\Column
{
    /**
     * @var \Magento\Framework\UrlInterface
     **/
    protected $urlBuilder;

    /**
     * __construct.
     *
     * @param \Magento\Framework\View\Element\UiComponent\ContextInterface $context
     * @param \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory
     * @param \Magento\Framework\UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\UiComponent\ContextInterface $context,
        \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory,
        \Magento\Framework\UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * prepareDataSource
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (!isset($dataSource['data']['items'])) {
            return $dataSource;
        }

        foreach ($dataSource['data']['items'] as &$item) {
            if (!isset($item['banner_id'])) {
                continue;
            }

            $item[$this->getData('name')] = [
                'edit'   => [
                    'href'  => $this->urlBuilder->getUrl(
                        'promobanners/index/edit',
                        [
                            'banner_id' => $item['banner_id']
                        ]
                    ),
                    'label' => __('Edit')
                ],
                'delete' => [
                    'href'    => $this->urlBuilder->getUrl(
                        'promobanners/index/delete',
                        [
                            'banner_id' => $item['banner_id']
                        ]
                    ),
                    'label'   => __('Delete'),
                    'confirm' => [
                        'title'   => __('Delete "%1"', $item['title']),
                        'message' => __('Are you sure you wan\'t to delete a "%1"?', $item['title'])
                    ]
                ]
            ];
        }

        return $dataSource;
    }
}
