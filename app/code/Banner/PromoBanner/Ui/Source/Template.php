<?php

namespace Banner\PromoBanner\Ui\Source;

class Template implements \Magento\Framework\Data\OptionSourceInterface
{
    const TEMPLATE_TYPE_SIMPLE = 0;
    const TEMPLATE_TYPE_DETAILED = 1;

    /**
     * @inheritDoc
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => self::TEMPLATE_TYPE_SIMPLE,
                'label' => __('Simple')
            ],
            [
                'value' => self::TEMPLATE_TYPE_DETAILED,
                'label' => __('Detailed')
            ]
        ];
    }
}
