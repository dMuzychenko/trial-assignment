<?php

namespace Banner\PromoBanner\Ui\Component\Form\Field;

use Magento\Framework\Stdlib\ArrayManager;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;

class DateTime extends \Magento\Ui\Component\Form\Field
{
    /**
     * @var ArrayManager
     */
    private $arrayManager;

    /**
     * DateTime constructor.
     * @param ArrayManager $arrayManager
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ArrayManager $arrayManager,
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = []
    ) {
        $this->arrayManager = $arrayManager;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * @inheritDoc
     */
    public function prepare()
    {
        parent::prepare();

        $config = $this->getData('config');
        $config = $this->arrayManager->set(
            'options/dateFormat',
            $config,
            'y-MM-dd'
        );
        $config = $this->arrayManager->set(
            'options/storeLocale',
            $config,
            'en_AU'
        );

        $this->setData('config', $config);
    }
}
