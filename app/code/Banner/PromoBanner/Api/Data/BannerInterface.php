<?php
namespace Banner\PromoBanner\Api\Data;

/**
 * Interface BannerInterface
 *
 * @api
 */
interface BannerInterface
{
    const BANNER_ID = 'banner_id';
    const TITLE = 'title';
    const SHOW_ONCE = 'show_once';
    const DATE_START = 'date_start';
    const DATE_END = 'date_end';
    const TEXT_FOR_LINK = 'text_for_link';
    const BANNER_TEXT = 'banner_text';
    const POPUP_TEXT = 'popup_text';
    const STORE_ID = 'store_id';

    /**
     * @return mixed
     */
    public function getBannerId();

    /**
     * @return mixed
     */
    public function getTitle();

    /**
     * @return mixed
     */
    public function getDateStart();

    /**
     * @return mixed
     */
    public function getDateEnd();

    /**
     * @return mixed
     */
    public function getStoreId();

    /**
     * @param $bannerId
     * @return $this
     */
    public function setBannerId($bannerId);

    /**
     * @param $title
     * @return $this
     */
    public function setTitle($title);

    /**
     * @param $showOnce
     * @return $this
     */
    public function setShowOnce($showOnce);

    /**
     * @param $dateStart
     * @return $this
     */
    public function setDateStart($dateStart);

    /**
     * @param $dateEnd
     * @return $this
     */
    public function setDateEnd($dateEnd);

    /**
     * @param $bannerText
     * @return $this
     */
    public function setBannerText($text);

    /**
     * @param $popupText
     * @return $this
     */
    public function setPopupText($text);

    /**
     * @param $storeId
     * @return $this
     */
    public function setStoreId($storeId);

    /**
     * @param $stores
     * @return $this
     */
    public function setStores($stores);
}
