<?php

namespace Banner\PromoBanner\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;

class Create extends Action
{
    /**
     * @return void
     */
    public function execute()
    {
        $this->_forward('edit');
    }
}
