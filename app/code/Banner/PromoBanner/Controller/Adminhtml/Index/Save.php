<?php

namespace Banner\PromoBanner\Controller\Adminhtml\Index;

use Banner\PromoBanner\Api\BannerRepositoryInterface;
use Banner\PromoBanner\Model\Banner;
use Banner\PromoBanner\Model\BannerFactory;
use Magento\Backend\App\Action;

class Save extends Action
{
    /**
     * @var BannerRepositoryInterface
     */
    private $bannerRepository;
    /**
     * @var BannerFactory
     */
    private $bannerFactory;

    /**
     * Save constructor.
     * @param Action\Context $context
     * @param BannerRepositoryInterface $bannerRepository
     * @param BannerFactory $bannerFactory
     */
    public function __construct(
        Action\Context $context,
        BannerRepositoryInterface $bannerRepository,
        BannerFactory $bannerFactory
    ) {
        parent::__construct($context);
        $this->bannerRepository = $bannerRepository;
        $this->bannerFactory = $bannerFactory;
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();

        if (!$data) {
            $this->messageManager->addErrorMessage(__('Cannot save banner'));
            return $this->_redirect('*/*/');
        }

        try {
            /** @var Banner $banner */
            $banner = $this->bannerFactory->create();

            if ($bannerId = (int)$this->getRequest()->getParam('banner_id')) {
                $banner = $this->bannerRepository->getById($bannerId);
                if ($bannerId != $banner->getId()) {
                    throw new \Magento\Framework\Exception\LocalizedException(__('The wrong item is specified.'));
                }
            }

            $banner->setData($data);
            $this->bannerRepository->save($banner);

            $this->messageManager->addSuccessMessage(__('The banner has been saved.'));

            if ($this->getRequest()->getParam('back')) {
                return $this->_redirect(
                    '*/*/edit',
                    ['banner_id' => $banner->getId(), '_current' => true]
                );
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            if ($bannerId = (int)$this->getRequest()->getParam('banner_id')) {
                return $this->_redirect('*/*/edit', ['banner_id' => $bannerId]);
            }

            return $this->_redirect('*/*/create');
        }

        return $this->_redirect('*/*/');
    }
}
