<?php

namespace Banner\PromoBanner\Controller\Adminhtml\Index;

use Banner\PromoBanner\Api\BannerRepositoryInterface;
use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Psr\Log\LoggerInterface;

class Delete extends Action
{
    /**
     * @var BannerRepositoryInterface
     */
    private $bannerRepository;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Delete constructor.
     * @param Action\Context $context
     * @param BannerRepositoryInterface $bannerRepository
     * @param LoggerInterface $logger
     */
    public function __construct(
        Action\Context $context,
        BannerRepositoryInterface $bannerRepository,
        LoggerInterface $logger
    ) {
        parent::__construct($context);
        $this->bannerRepository = $bannerRepository;
        $this->logger = $logger;
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        if ($bannerId = $this->getRequest()->getParam('banner_id')) {
            try {
                $this->bannerRepository->deleteById($bannerId);
                $this->messageManager->addSuccessMessage(__('You\'ve deleted the banner.'));
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(
                    __('We can\'t delete banner right now. Please review the log and try again.')
                );
                $this->logger->critical((string)$e);
            }
        }
        $this->_redirect('*/*/');
    }
}
