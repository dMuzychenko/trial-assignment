<?php

namespace Banner\PromoBanner\Controller\Adminhtml\Index;

use Banner\PromoBanner\Api\BannerRepositoryInterface;
use Magento\Backend\App\Action;

class Edit extends Action
{
    const ADMIN_RESOURCE = 'Banner_PromoBanner::edit';

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    private $resultPageFactory;
    /**
     * @var BannerRepositoryInterface
     */
    private $bannerRepository;

    /**
     * Edit constructor.
     * @param Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param BannerRepositoryInterface $bannerRepository
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        BannerRepositoryInterface $bannerRepository
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->bannerRepository = $bannerRepository;
        parent::__construct($context);
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Banner_PromoBanner::grid');

        if ($bannerId = (int) $this->getRequest()->getParam('banner_id')) {
            try {
                $banner = $this->bannerRepository->getById($bannerId);
                $resultPage->getConfig()->getTitle()->prepend(__('Edit Banner'));
            } catch (\Magento\Framework\Exception\NoSuchEntityException $exception) {
                $this->messageManager->addErrorMessage(__('This banner no longer exists.'));

                return $this->_redirect('*/*/index');
            }
        } else {
            $resultPage->getConfig()->getTitle()->prepend(__('New Banner'));
        }

        return $resultPage;
    }
}
