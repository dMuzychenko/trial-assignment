<?php
namespace Banner\PromoBanner\Model;

use Banner\PromoBanner\Api\Data\BannerInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Banner
 */
class Banner extends AbstractModel implements
    BannerInterface,
    IdentityInterface
{
    const CACHE_TAG = 'banner_promobanner_banner';

    /**
     * @var string
     */
    protected $_idFieldName = self::BANNER_ID;

    /**
     * Init
     */
    protected function _construct() // phpcs:ignore PSR2.Methods.MethodDeclaration
    {
        $this->_init(\Banner\PromoBanner\Model\ResourceModel\Banner::class);
    }

    /**
     * @inheritDoc
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @inheritDoc
     */
    public function getBannerId()
    {
        return $this->_getData(self::BANNER_ID);
    }

    /**
     * @inheritDoc
     */
    public function getTitle()
    {
        return $this->_getData(self::TITLE);
    }

    /**
     * @inheritDoc
     */
    public function getDateStart()
    {
        return $this->_getData(self::DATE_START);
    }

    /**
     * @inheritDoc
     */
    public function getDateEnd()
    {
        return $this->_getData(self::DATE_END);
    }

    /**
     * @inheritDoc
     */
    public function getStoreId()
    {
        return $this->_getData(self::STORE_ID);
    }

    /**
     * @inheritDoc
     */
    public function getStores()
    {
        return $this->hasData('stores') ? $this->_getData(self::STORES) : $this->_getData(self::STORE_ID);
    }

    /**
     * @inheritDoc
     */
    public function setBannerId($bannerId)
    {
        $this->setData(self::BANNER_ID, $bannerId);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setTitle($title)
    {
        $this->setData(self::TITLE, $title);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setShowOnce($showOnce)
    {
        $this->setData(self::SHOW_ONCE, $showOnce);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setDateStart($dateStart)
    {
        $this->setData(self::DATE_START, $dateStart);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setDateEnd($dateEnd)
    {
        $this->setData(self::DATE_END, $dateEnd);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setBannerText($text)
    {
        $this->setData(self::BANNER_TEXT, $text);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setPopupText($text)
    {
        $this->setData(self::POPUP_TEXT, $text);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setStoreId($storeId)
    {
        $this->setData(self::STORE_ID, $storeId);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setStores($stores)
    {
        $this->setData(self::STORES, $stores);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function beforeSave()
    {
        $this->setDateStart(
            $this->getPreparedToSaveDateString(
                $this->getDateStart()
            )
        );
        $this->setDateEnd(
            $this->getPreparedToSaveDateString(
                $this->getDateEnd()
            )
        );
        return parent::beforeSave();
    }

    /**
     * @param string $isoDate UTC formatted ISO string (from javascript)
     * @return string
     */
    private function getPreparedToSaveDateString($isoDate)
    {
        $dateTime = new \DateTime($isoDate, new \DateTimeZone('Europe/London'));
        return $dateTime->format(\Magento\Framework\Stdlib\DateTime::DATETIME_PHP_FORMAT);
    }
}
