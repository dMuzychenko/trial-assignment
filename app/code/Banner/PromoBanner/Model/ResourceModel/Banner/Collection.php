<?php
namespace Banner\PromoBanner\Model\ResourceModel\Banner;

/**
 * Class Collection
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var \Magento\Framework\EntityManager\MetadataPool
     */
    private $metadataPool;

    /**
     * Collection constructor.
     * @param \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\EntityManager\MetadataPool $metadataPool
     * @param \Magento\Framework\DB\Adapter\AdapterInterface|null $connection
     * @param \Magento\Framework\Model\ResourceModel\Db\AbstractDb|null $resource
     */
    public function __construct(
        \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\EntityManager\MetadataPool $metadataPool,
        \Magento\Framework\DB\Adapter\AdapterInterface $connection = null,
        \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource = null
    ) {
        $this->storeManager = $storeManager;
        $this->metadataPool = $metadataPool;
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $connection, $resource);
    }

    /**
     * Init
     */
    protected function _construct() // phpcs:ignore PSR2.Methods.MethodDeclaration
    {
        $this->_init(\Banner\PromoBanner\Model\Banner::class, \Banner\PromoBanner\Model\ResourceModel\Banner::class);
        $this->_setIdFieldName($this->getResource()->getIdFieldName());

        $this->_map['fields']['store'] = 'store_table.store_id';
        $this->_map['fields']['banner_id'] = 'main_table.banner_id';
    }

    /**
     * _afterLoad
     *
     * @return self
     */
    protected function _afterLoad()
    {
        $entityMetadata = $this->metadataPool->getMetadata(\Banner\PromoBanner\Api\Data\BannerInterface::class);
        $linkField = $entityMetadata->getLinkField();
        $linkedIds = $this->getColumnValues($linkField);

        if (count($linkedIds)) {
            $this->addInformationAboutStores($linkedIds);
            $this->addUniqueIds($linkedIds);
        }

        return parent::_afterLoad();
    }

    /**
     * addInformationAboutStores
     *
     * @param array $linkedIds
     * @return void
     */
    protected function addInformationAboutStores($linkedIds)
    {
        $entityMetadata = $this->metadataPool->getMetadata(\Banner\PromoBanner\Api\Data\BannerInterface::class);
        $linkField = $entityMetadata->getLinkField();
        $tableName = 'banner_promo_banners_store';

        $connection = $this->getConnection();
        $select = $connection->select()->from(['bpbs' => $this->getTable($tableName)])
            ->where('bpbs.' . $linkField . ' IN (?)', $linkedIds);
        $result = $connection->fetchAll($select);
        if ($result) {
            $storesData = [];
            foreach ($result as $storeData) {
                $storesData[$storeData[$linkField]][] = $storeData['store_id'];
            }

            foreach ($this as $item) {
                $linkedId = $item->getData($linkField);
                if (!isset($storesData[$linkedId])) {
                    continue;
                }
                $storeIdKey = array_search(\Magento\Store\Model\Store::DEFAULT_STORE_ID, $storesData[$linkedId], true);
                if ($storeIdKey !== false) {
                    $stores = $this->storeManager->getStores(false, true);
                    $storeId = current($stores)->getId();
                    $storeCode = key($stores);
                } else {
                    $storeId = current($storesData[$linkedId]);
                    $storeCode = $this->storeManager->getStore($storeId)->getCode();
                }
                $item->setData('_first_store_id', $storeId);
                $item->setData('store_code', $storeCode);
                $item->setData('store_id', $storesData[$linkedId]);
            }
        }
    }

    /**
     * @param array $linkedIds
     */
    protected function addUniqueIds($linkedIds)
    {
        foreach ($this as $item) {
            $stores = $item->getStoreId() ?? [];
            $storesStr = implode('-', $stores);
            $bannerId = $item->getId();
            $id = "banner_{$storesStr}_{$bannerId}";

            $item->setUniqueId(md5($id));
        }
    }

    /**
     * addStoreFilter
     *
     * @param int|array|\Magento\Store\Model\Store $store
     * @param bool $withAdmin
     * @return self
     */
    public function addStoreFilter($store, $withAdmin = true)
    {
        if ($store instanceof \Magento\Store\Model\Store) {
            $store = [$store->getId()];
        }

        if (!is_array($store)) {
            $store = [$store];
        }

        if ($withAdmin) {
            $store[] = \Magento\Store\Model\Store::DEFAULT_STORE_ID;
        }

        $this->addFilter('store', ['in' => $store], 'public');

        return $this;
    }

    /**
     * _renderFiltersBefore
     *
     * @return void
     */
    protected function _renderFiltersBefore()
    {
        $entityMetadata = $this->metadataPool->getMetadata(\Banner\PromoBanner\Api\Data\BannerInterface::class);
        $linkField = $entityMetadata->getLinkField();
        $tableName = 'banner_promo_banners_store';

        if ($this->getFilter('store')) {
            $this->getSelect()->join(
                ['store_table' => $this->getTable($tableName)],
                'main_table.' . $linkField . ' = store_table.' . $linkField,
                []
            )->group(
                'main_table.' . $linkField
            );

            parent::_renderFiltersBefore();
        }
    }
}
