<?php

namespace Banner\PromoBanner\Model\ResourceModel\Banner\Relation\Store;

class ReadHandler implements \Magento\Framework\EntityManager\Operation\ExtensionInterface
{
    /**
     * @var \Banner\PromoBanner\Model\ResourceModel\Banner
     */
    protected $resourceModel;

    /**
     * ReadHandler constructor.
     * @param \Banner\PromoBanner\Model\ResourceModel\Banner $resourceModel
     */
    public function __construct(
        \Banner\PromoBanner\Model\ResourceModel\Banner $resourceModel
    ) {
        $this->resourceModel = $resourceModel;
    }

    /**
     * @inheritDoc
     */
    public function execute($entity, $arguments = [])
    {
        if ($entity->getId()) {
            $stores = $this->resourceModel->lookupStoreIds((int)$entity->getId());
            $entity->setData('store_id', $stores);
            $entity->setData('stores', $stores);
        }
        return $entity;
    }
}
