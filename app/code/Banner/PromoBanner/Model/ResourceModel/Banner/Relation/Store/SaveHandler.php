<?php

namespace Banner\PromoBanner\Model\ResourceModel\Banner\Relation\Store;


class SaveHandler implements \Magento\Framework\EntityManager\Operation\ExtensionInterface
{
    /**
     * @var \Banner\PromoBanner\Model\ResourceModel\Banner
     */
    protected $resourceModel;
    /**
     * @var \Magento\Framework\EntityManager\MetadataPool
     */
    protected $metadataPool;

    /**
     * SaveHandler constructor.
     * @param \Banner\PromoBanner\Model\ResourceModel\Banner $resourceModel
     * @param \Magento\Framework\EntityManager\MetadataPool $metadataPool
     */
    public function __construct(
        \Banner\PromoBanner\Model\ResourceModel\Banner $resourceModel,
        \Magento\Framework\EntityManager\MetadataPool $metadataPool
    ) {
        $this->resourceModel = $resourceModel;
        $this->metadataPool = $metadataPool;
    }

    /**
     * @inheritDoc
     */
    public function execute($entity, $arguments = [])
    {
        $entityMetadata = $this->metadataPool->getMetadata(\Banner\PromoBanner\Api\Data\BannerInterface::class);
        $linkField = $entityMetadata->getLinkField();
        $identifiedField = $entityMetadata->getIdentifierField();

        $connection = $entityMetadata->getEntityConnection();

        $oldStores = $this->resourceModel->lookupStoreIds((int)$entity->getId());
        $newStores = (array)$entity->getStores();

        $table = $this->resourceModel->getTable('banner_promo_banners_store');

        $delete = array_diff($oldStores, $newStores);
        if ($delete) {
            $where = [
                $linkField . ' = ?' => (int)$entity->getData($identifiedField),
                'store_id IN (?)' => $delete,
            ];
            $connection->delete($table, $where);
        }

        $insert = array_diff($newStores, $oldStores);
        if ($insert) {
            $data = [];
            foreach ($insert as $storeId) {
                $data[] = [
                    $linkField => (int)$entity->getData($identifiedField),
                    'store_id' => (int)$storeId,
                ];
            }
            $connection->insertMultiple($table, $data);
        }

        return $entity;
    }
}
