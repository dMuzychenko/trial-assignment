<?php
namespace Banner\PromoBanner\Model\ResourceModel;

/**
 * Class Banner
 */
class Banner extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * @var \Magento\Framework\EntityManager\MetadataPool
     */
    protected $metadataPool;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var \Magento\Framework\EntityManager\EntityManager
     */
    protected $entityManager;

    /**
     * Banner constructor.
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\EntityManager\EntityManager $entityManager
     * @param \Magento\Framework\EntityManager\MetadataPool $metadataPool
     * @param string $connectionName
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\EntityManager\EntityManager $entityManager,
        \Magento\Framework\EntityManager\MetadataPool $metadataPool,
        $connectionName = null
    ) {
        $this->storeManager = $storeManager;
        $this->entityManager = $entityManager;
        $this->metadataPool = $metadataPool;
        parent::__construct($context, $connectionName);
    }

    /**
     * Init
     */
    protected function _construct() // phpcs:ignore PSR2.Methods.MethodDeclaration
    {
        $this->_init('banner_promo_banners', 'banner_id');
    }

    /**
     * @param int $id
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function lookupStoreIds($id)
    {
        $connection = $this->getConnection();

        $entityMetadata = $this->metadataPool->getMetadata(\Banner\PromoBanner\Api\Data\BannerInterface::class);
        $linkField = $entityMetadata->getLinkField();
        $identifiedField = $entityMetadata->getIdentifierField();

        $select = $connection->select()
            ->from(['bpbs' => $this->getTable('banner_promo_banners_store')], 'store_id')
            ->join(
                ['bpb' => $this->getMainTable()],
                'bpbs.' . $linkField . ' = bpb.' . $identifiedField,
                []
            )
            ->where('bpb.' . $identifiedField . ' = :banner_id');

        return $connection->fetchCol($select, ['banner_id' => (int)$id]);
    }

    /**
     * save
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return self
     */
    public function save(\Magento\Framework\Model\AbstractModel $object)
    {
        $this->entityManager->save($object);
        return $this;
    }

    /**
     * delete
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return self
     */
    public function delete(\Magento\Framework\Model\AbstractModel $object)
    {
        $this->entityManager->delete($object);
        return $this;
    }
}
