<?php

namespace Banner\PromoBanner\Model\DataProvider;

use Banner\PromoBanner\Model\ResourceModel\Banner\CollectionFactory;

class Form extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var array|null
     */
    private $loadedData;
    /**
     * Form constructor.
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $collectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * @inheritDoc
     */
    public function getData()
    {
        if ($this->loadedData !== null) {
            return $this->loadedData;
        }

        $this->loadedData = [];

        $banners = $this->getCollection()->getItems();

        foreach ($banners as $banner) {
            $this->loadedData[$banner->getId()] = $banner->getData();
        }

        return $this->loadedData;
    }
}
