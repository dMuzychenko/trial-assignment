<?php

namespace Banner\PromoBanner\Model\DataProvider;

use Banner\PromoBanner\Model\ResourceModel\Banner\CollectionFactory;

class Listing extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * Listing constructor.
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $collectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

}
