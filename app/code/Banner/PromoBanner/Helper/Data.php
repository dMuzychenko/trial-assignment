<?php

namespace Banner\PromoBanner\Helper;

use Banner\PromoBanner\Api\Data\BannerInterface;
use Banner\PromoBanner\Ui\Source\Template;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;

class Data extends AbstractHelper
{
    /**
     * Data constructor.
     * @param Context $context
     */
    public function __construct(Context $context)
    {
        parent::__construct($context);
    }

    /**
     * @param $startDate
     * @param $endDate
     * @return bool
     */
    public function isBannerActive($startDate, $endDate)
    {
        $start = \DateTime::createFromFormat('Y-m-d H:i:s', $startDate, (new \DateTimeZone('GMT')));
        $end = \DateTime::createFromFormat('Y-m-d H:i:s', $endDate, (new \DateTimeZone('GMT')));

        $shouldCheck = $end !== false;

        if (!$shouldCheck) {
            return false;
        }

        $currentTime = time();
        $startValid = !$start || $currentTime >= $start->getTimestamp();

        return $startValid && $currentTime <= $end->getTimestamp();
    }
}
